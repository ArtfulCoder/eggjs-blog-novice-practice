# mydemo eggjs
> 新手练习项目
## 用的技术栈
eggjs mysql nunjucks crypto-js（后端渲染）
## 实现的功能
* 后台管理
* 文章展示
* 登录拦截
* 后台账号密码加密
## 后台密码账号
* 账号：14799941581@139.com
* 密码：M2000925.

## QuickStart

<!-- add docs here for user -->

see [egg docs][egg] for more detail.

### Development

```bash
$ npm i
$ npm run dev
$ open http://localhost:7001/
```

### Deploy

```bash
$ npm start
$ npm stop
```

### npm scripts

- Use `npm run lint` to check code style.
- Use `npm test` to run unit test.
- Use `npm run autod` to auto detect dependencies upgrade, see [autod](https://www.npmjs.com/package/autod) for more detail.


[egg]: https://eggjs.org