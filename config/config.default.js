/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};
  config.sequelize = {
    dialect: 'mysql',
    database: 'blog',
    host: 'localhost',
    port: 3306,
    username:'root',
    password:'123456'
    
  };
  config.view = {
    defaultViewEngine: 'nunjucks',
    mapping: {
      '.html': 'nunjucks',
    },
  };
  
  config.multipart = {
    // will append to whilelist
    fileExtensions: [
      '.foo',
      '.apk',
    ],
  };

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1634193756277_7673';

  // add your middleware config here
  config.middleware = ['auth','adminauth'];
  config.adminauth = {
    match:'/articlelist'
  }


  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  return {
    ...config,
    ...userConfig,
  };
};
