// app/extend/application.js

var CryptoJS = require("crypto-js");
const secret_key ='ABCDEF1234123412'


module.exports = {
  formathtmlstr(param) {
    // this 就是 app 对象，在其中可以调用 app 上的其他方法，或访问属性
    return param.replace(/<[^>]+>/g, "").substr(0, 40) + "....";//去掉所有的html标记 
  },
  istype(type) {
    // this 就是 app 对象，在其中可以调用 app 上的其他方法，或访问属性
    switch (type) {
      case 1:
        return "ئالدى بەت"
        break;
      case 2:
        return "ئارقى بەت"
        break;
      case 3:
        return "ئەپچاق"
        break;
      case 4:
        return "ماتىريال"
        break;
      case 5:
        return "يازمىلار"
        break;
      default:
        break;
    }
  },
  //两边引号
  replacestr(content){
    console.log(content);
    return content.replace("\"","").replace("\"","")
  },
  //加密方法
  Encrypt(word) {
    
    return CryptoJS.AES.encrypt(word, secret_key).toString();
  },
  //解密方式
  Decrypt(word) {
    let bytes  = CryptoJS.AES.decrypt(word, secret_key);
    return bytes.toString(CryptoJS.enc.Utf8);
  }

};
