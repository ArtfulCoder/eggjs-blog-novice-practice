const Controller = require('egg').Controller;
class LoginController extends Controller {
    //登录页面
    async index(){
        const { ctx } = this;
        await ctx.render('login');
      }
    async login(){
      const {ctx} =this
      let {email,password} =ctx.request.body
      let classlist = await this.app.model.Admin.findAll({
        where:{
          id:1
        }
      });
    
      if (  email==ctx.helper.Decrypt(classlist[0].email) && password==ctx.helper.Decrypt(classlist[0].password)) {
        ctx.session.userinfo="email"
        ctx.redirect("/articlelist")
      } else {
        ctx.redirect('/login')
      }
      
    }
    async loginout(){
      this.ctx.session.userinfo=null
      this.ctx.redirect("/login")
    }
    }

module.exports = LoginController