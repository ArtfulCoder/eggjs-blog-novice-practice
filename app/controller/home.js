'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    let articlelist = await this.app.model.Artcile.findAll()
    console.log(articlelist);
    const { ctx } = this;
    await ctx.render('article',{
      articlelist:articlelist
    });
  }
}

module.exports = HomeController;
