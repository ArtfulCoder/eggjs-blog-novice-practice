'use strict';
	//引入egg的controller
  const fs = require('fs');
  const path = require('path');
  const Controller = require('egg').Controller;
  const pump = require('mz-modules/pump');
class AddarticleController extends Controller {
  //后台文章列表
  async articlelist(){
    let {ctx} = this
    let articlelist = await this.app.model.Artcile.findAll()
    await ctx.render("articleedit",{articlelist})
  }
  //文章添加路由
  async editartilce(){
    let {ctx} = this
    await  ctx.render("edit")
  }
  async AddContent() {
        try {
          const stream = await this.ctx.getFileStream();
          const filename = Date.now().toString() + path.extname(stream.filename).toLowerCase();
          const target = path.join(this.config.baseDir, 'app/public/uploads', filename);
          const writeStream = fs.createWriteStream(target);
          await pump(stream, writeStream);
          const { ctx } = this;
          // 获取post 请求参数
          console.log(stream,"stream");
          let {title,author,type,content}=stream.fields
          let cover = target.split('app')[1]
          await this.app.model.Artcile.create({
            title:title,
            author:author,
            type:Number(type),
            cover:cover,
            content:content
          })
          this.ctx.redirect("/articlelist")
        } catch (error) {
          this.ctx.body="添加失败"
        }
  }
    
  
}

module.exports = AddarticleController;
