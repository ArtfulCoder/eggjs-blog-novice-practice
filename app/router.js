'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  router.get('/login',controller.login.index);
  router.post('/login',controller.login.login);
  app.post('/login/loginout',controller.login.loginout)
  router.get('/articlelist',controller.addarticle.articlelist);
  router.get('/articlelist/edit',controller.addarticle.editartilce);
  app.post('/addart',controller.addarticle.AddContent)
  app.get('/articlecn',controller.articlecn.index)
};
