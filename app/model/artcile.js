'use strict';

module.exports = app => {
  const { STRING, INTEGER, DATE } = app.Sequelize;

  const Artilce = app.model.define('article', {
    id: { type: INTEGER, primaryKey: true, autoIncrement: true },
    title: STRING(100),
    author: STRING(60),
    type:INTEGER,
    cover:STRING(500),
    content:STRING(10000),
    created_at: DATE,
    updated_at: DATE,
  });

  return Artilce;
};