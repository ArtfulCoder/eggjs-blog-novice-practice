'use strict';

module.exports = app => {
  const { STRING, INTEGER, DATE } = app.Sequelize;

  const Admin = app.model.define('admin', {
    id: { type: INTEGER, primaryKey: true, autoIncrement: true },
    email:STRING,
    password:STRING,
    created_at: DATE,
    updated_at: DATE,
  });

  return Admin;
};